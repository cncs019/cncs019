#include <stdio.h>

int main()
{
    int i;
    char str[50];
    printf("Enter the characters\n");
    while(str[i]!='\n')
    {
        int character=getchar();
        str[i]=character;
        i++;
    }
    i=i-1;
    str[i]='\0';
    while(str[i]!='\0')
    {
        putchar(str[i]);
    }
    printf("The length of the string is %d",i);
    return 0;
}